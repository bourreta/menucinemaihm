import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../auth/auth.guard';
import {ProductSelectionComponent} from "../product/product-selection/product-selection.component";
import {HomeComponent} from "../home/home.component";
import {BillComponent} from '../bill/bill.component';

const routes: Routes = [
    {
        path: 'homepage',
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'bill',
        component: BillComponent,
        canActivate: [AuthGuard]
    }
];


@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class HomeRoutingModule {
}
