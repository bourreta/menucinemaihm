import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TmdbService} from '../../services/tmdb.service';
import {CreditsResponse, MovieResponse, Trailers} from '../../tmdb-data/Movie';
import {RecommandationService} from '../../recommandation/recommandation.service';
import {ProductType} from '../../enum/ProductType';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Movie} from '../../product/movie/Movie';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BasketService} from "../../basket/basket.service";
import {MatSnackBar} from '@angular/material';
import {StepperService} from '../../stepper-perso/stepper.service';



@Component({
  selector: 'app-movie-info',
  templateUrl: './movie-info.component.html',
  styleUrls: ['./movie-info.component.scss']
})
export class MovieInfoComponent implements OnInit {
  private safeUrl: SafeResourceUrl;


  constructor(public dialogRef: MatDialogRef<MovieInfoComponent>,
              private tmdbService: TmdbService,
              private route: ActivatedRoute,
              private recommandationService: RecommandationService,
              private sanitizer: DomSanitizer,
              private router: Router,
              private basketService: BasketService,
              public snackBar: MatSnackBar,
              public stepper: StepperService,
              @Inject(MAT_DIALOG_DATA) public data: number,
  ) {
    this.movie = {
      adult: false,
      genres: [],
      imdb_id: '',
      overview: '',
      poster_path: '',
      release_date: '',
      tagline: '',
      title: '',
      vote_average: 0,
      vote_count: 0
    };

    this.castNCrew = {crew: [], cast: []};
    this.trailer = {
      id: 0,
      key: ''
    };

  }
  movie: MovieResponse;
  idMovie: number;
  castNCrew: CreditsResponse;
  movieClass: Movie;
  trailer: Trailers;

  // shld return the youtube link to show in the src
  int; i = 0;

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      // assigning this.idMovie to the passed movieId in param
      this.idMovie = this.data;
      this.tmdbService.getMovie(this.idMovie).then((data) => {
        this.movie = data;
        this.movieClass = Movie.fromData(data);
      });
      this.tmdbService.getMovieCastAndCrew(this.idMovie).then((data) => {
        this.castNCrew = data;
      });
      this.tmdbService.getMovietrailer(this.idMovie).then((data) => {
        this.trailer = data;
        //  console.log("HEOOOO " + this.tmdbService.getYtubelink(data) )
        this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.tmdbService.getYtubelink(data) + '?autoplay=1');
      });
    });
  }




  getBackDropPath() {
    return this.tmdbService.getBackDropPath(this.movie);
  }


  getPosterPath() {
    return this.tmdbService.getPosterPath(this.movie);
  }

  getGivenType() {
    return this.recommandationService.getGivenType(ProductType.Movie);
  }

  getSearchType() {
    return this.recommandationService.getSearchType(ProductType.Movie);
  }

  // a finir fab
  addToCart(movie) {
    this.basketService.addMovie(movie);
    this.close();
    this.stepper.desactivEtat();
    this.router.navigate(['/product']);
    this.snackBar.open('Le film ' + this.movie.title + ' a été ajouté au panier.', '5€', {
      duration: 8000,
    });
  }


  close() {
    this.dialogRef.close();
  }
}
