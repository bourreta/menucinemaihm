import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarPizzaComponent } from './nav-bar-pizza.component';

describe('NavBarPizzaComponent', () => {
  let component: NavBarPizzaComponent;
  let fixture: ComponentFixture<NavBarPizzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavBarPizzaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarPizzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
