import {FoodInterface} from '../../interface/FoodInterface';
import {ProductType} from '../../enum/ProductType';
import {Product} from '../class/Product';
import {log} from "util";


// Modif fabien
export class Food extends Product implements FoodInterface {
    public description: string;
    public nom: string;
    public photo: string;
    public categorie: number;
    public ingredients: string [];
    public  extraFromage: number;
    public extraBacon: number;
    public typePate: string;

    static fromData(data: FoodInterface) {
        const {description, id, nom, prix, type, photo, categorie, ingredients, extraBacon, extraFromage, typePate} = data;
        let ingredientsRename;
        if (ingredients !== undefined) {
            ingredientsRename = ingredients.map(value => value.split('_').join(' ').split('lll').join('&').split('ppp').join('`'));
        }

        return new this(id, prix, description, nom, type, photo, categorie, ingredientsRename, extraBacon, extraFromage, typePate);
    }

    constructor(id: number, prix: number, description: string, nom: string, type: ProductType, photo: string = '', categorie: number = 0, ingredients: string [] = null, extraBacon: number = 0, extraFromage: number = 0, typePate: string ='') {
        super(id, prix, type, prix,0,0,'');
        this.description = description;
        this.nom = nom;
        this.photo = photo;
        this.categorie = categorie;
        this.ingredients = ingredients;
        this.extraBacon = extraBacon;
        this.extraFromage = extraFromage;
        this.typePate = typePate;
    }


}
