import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TmdbService} from '../../services/tmdb.service';
import {CreditsResponse, MovieResponse, Trailers} from '../../tmdb-data/Movie';
import {BasketService} from '../../basket/basket.service';
import {Movie} from './Movie';
import {RecommandationService} from '../../recommandation/recommandation.service';
import {ProductType} from '../../enum/ProductType';
import {Product} from '../class/Product';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {MatSnackBar} from '@angular/material';

@Component({
    selector: 'app-movie',
    templateUrl: './movie.component.html',
    styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
    private safeUrl: SafeResourceUrl;


     constructor(private tmdbService: TmdbService,
                 private route: ActivatedRoute,
                 private basketService: BasketService,
                 private recommandationService: RecommandationService,
                 private sanitizer: DomSanitizer,
                 public snackBar: MatSnackBar) {
        this.movie = {
            adult: false,
            genres: [],
            imdb_id: '',
            overview: '',
            poster_path: '',
            release_date: '',
            tagline: '',
            title: '',
            vote_average: 0,
            vote_count: 0
        };

        this.castNCrew = {crew: [], cast: []};
        this.trailer = {
            id: 0,
         key: ''
        };

    }
    movie: MovieResponse;
    idMovie: number;
    castNCrew: CreditsResponse;
    movieClass: Movie;
    trailer: Trailers;

    // shld return the youtube link to show in the src
    int; i = 0;

    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            this.idMovie = +params.get('id');
            this.tmdbService.getMovie(this.idMovie).then((data) => {
                this.movie = data;
                this.movieClass = Movie.fromData(data);
            });
            this.tmdbService.getMovieCastAndCrew(this.idMovie).then((data) => {
                this.castNCrew = data;
            });
            this.tmdbService.getMovietrailer(this.idMovie).then((data) => {
                this.trailer = data;
              //  console.log("HEOOOO " + this.tmdbService.getYtubelink(data) )
                this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.tmdbService.getYtubelink(data)+ '?autoplay=1');
            });
        });
    }



    addToCart() {
        this.basketService.addMovie(Movie.fromData(this.movie));
        this.snackBar.open('Le film a été ajouté au panier.', '5€', {
            duration: 4000,
        });
    }

    getBackDropPath() {
        return this.tmdbService.getBackDropPath(this.movie);
    }


    getPosterPath() {
        return this.tmdbService.getPosterPath(this.movie);
    }

    getGivenType() {
        return this.recommandationService.getGivenType(ProductType.Movie);
    }

    getSearchType() {
        return this.recommandationService.getSearchType(ProductType.Movie);
    }
   // getYtubelink() {
        // console.log(this.tmdbService.getYtubelink(this.movie));
// await this.tmdbService.getYtubelink(this.movie)
       // this.safeURL = await this.tmdbService.getYtubelink(this.movie);
        //    console.log("HAKALAAJLAJSLAJSLAJS : " + this.safeURL);
       // return this.tmdbService.getYtubelink(this.trailer);
    //    return this.sanitizer.bypassSecurityTrustResourceUrl(this.tmdbService.getYtubelink(this.trailer)+'?autoplay=1');

   // }


}
