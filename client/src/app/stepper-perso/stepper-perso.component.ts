import {Component, Input, OnInit} from '@angular/core';
import {BasketService} from '../basket/basket.service';
import {Router} from '@angular/router';
import {ProductGroupInterface} from '../interface/ProductInterface';
import {CommandeDialogComponent} from '../dialogs/commande-dialog/commande-dialog.component';
import {MatDialog} from '@angular/material';
import {AddEventInterface} from '../interface/AddEventInterface';
import {AddEventType} from '../enum/AddEventType';
import {StepperService} from './stepper.service';

@Component({
  selector: 'app-stepper-perso',
  templateUrl: './stepper-perso.component.html',
  styleUrls: ['./stepper-perso.component.scss']
})
export class StepperPersoComponent implements OnInit {

  @Input() private eventType: AddEventType;
  private errorFood: boolean;

  constructor(private basket: BasketService,
              private router: Router,
              public dialog: MatDialog,
              public stepper: StepperService) {
    this.basket = basket;
  }

  ngOnInit() {
    this.initErrorFood();

  }
  getExistFilm() {
    if (this.basket.getExistMovie()) {
      return true;
    } else { return false; }
  }
  getExistFoods() {
    if (this.basket.getExistFoods()) {
      return true;
    } else { return false; }
  }
  removeMovie() {
    if (confirm("Etes-vous sûr de vouloir modifier votre film ? Cela supprimera celui que vous avez actuellement.")) {
      if (this.basket.getExistMovie()) {
        this.basket.removeMovieByIndex(0);
      }
      this.initFinish();
      this.router.navigate(['/']);
    }
  }
  setErrorFood() {
    this.stepper.activEtat();
  }
  initErrorFood() {
    this.stepper.desactivEtat();
  }
  getErrorFood() {
    return this.stepper.getEtat();
  }
  getFinish() {
    return this.stepper.getFinish();
  }
  initFinish() {
    this.stepper.desactivEtat();
  }
  validBasket() {
    this.stepper.activFinish();
    this.router.navigate(['/bill']);
  }
  modFoods() {
    this.initFinish();
    this.router.navigate(['/product']);
  }

}
