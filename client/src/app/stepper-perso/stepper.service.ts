import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StepperService {
  private etat: boolean;
  private finish: boolean;
  constructor() {
    this.initEtat();
  }

  initEtat() {
    this.etat = false;
    this.finish = false;
  }
  activEtat() {
    this.etat = true;
  }
  activFinish() {
    this.finish = true;
  }
  desactivEtat() {
    this.etat = false;
    this.finish = false;
  }
  getEtat() {
    return this.etat;
  }
  getFinish() {
    return this.finish;
  }
}
