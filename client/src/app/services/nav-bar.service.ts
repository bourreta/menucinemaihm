import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavBarService {
  private _searchString: string;
  private _searchRating: number;
  _searchRatingSubject = new Subject<number>();

  _SearchStringSubject = new Subject<string>();
  _hiddeSearchBar: boolean;
  _hiddeSearchBarSubject = new Subject<boolean>();
  private _catArr: string;
  _catArrSubject = new Subject<string>();

  private _monLien: string;
  _monLienSubject = new Subject<string>();



  constructor() {
    this._searchString = '';
    this._searchRating = 0;
    this._hiddeSearchBar = true;
    this._catArr = '';
    this.monLien = '';
  }

  get monLien(): string {
    return this.monLien;
  }

  set monLien(value: string) {
    this._monLien = value;
    this._monLienSubject.next(value);
  }

  get searchString(): string {
    return this._searchString;
  }

  set searchString(value: string) {
    this._searchString = value;
    this._SearchStringSubject.next(value);
  }

  get searchRating(): number {
    return this._searchRating;
  }

  set searchRating(value: number) {
    this._searchRating = value;
    this._searchRatingSubject.next(value);
    this._hiddeSearchBar = true;
    this._hiddeSearchBarSubject.next(true);
  }

  get categs(): string {
    return this._catArr;
  }

  set categs(value: string) {
    this._catArr = value;
    this._catArrSubject.next(value);
    this._hiddeSearchBar = true;
    this._hiddeSearchBarSubject.next(true);
  }
/*
  set horreur(value: boolean) {
    this._horreur = value;
    this._horreurSubject.next(value);
    this._hiddeSearchBar = value;
    this._hiddeSearchBarSubject.next(value);
  }
  set comedie(value: boolean) {
    this._comedie = value;
    this._comedieSubject.next(value);
    this._hiddeSearchBar = value;
    this._hiddeSearchBarSubject.next(value);
  }


  set aventure(value: boolean) {
    this._aventure = value;
    this._aventureSubject.next(value);
    this._hiddeSearchBar = value;
    this._hiddeSearchBarSubject.next(value);
  }


  set action(value: boolean) {
    this._action = value;
    this._actionSubject.next(value);
    this._hiddeSearchBar = value;
    this._hiddeSearchBarSubject.next(value);
  }
*/

}
