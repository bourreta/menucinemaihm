import {Component, OnInit} from '@angular/core';
import {BasketService} from './basket.service';
import {BasketInterface} from '../interface/BasketInterface';
import {FoodService} from '../product/food/food.service';
import {MenuService} from '../product/menu/menu.service';
import {Basket} from './Basket';
import {Router} from '@angular/router';
import {MatDialog, MatSnackBar} from '@angular/material';
import {GenericDialogModule} from '../dialogs/generic-dialog/generic-dialog.module';
import {CommandeDialogComponent} from '../dialogs/commande-dialog/commande-dialog.component';
import {GenericDialogComponent} from '../dialogs/generic-dialog/generic-dialog.component';
import {MovieInfoComponent} from "../dialogs/movie-info/movie-info.component";
import {ProductGroupInterface} from "../interface/ProductInterface";
import {StepperService} from '../stepper-perso/stepper.service';

@Component({
    selector: 'app-basket',
    templateUrl: './basket.component.html',
    styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {
    private basket: Basket;

    constructor(private basketService: BasketService,
                private foodService: FoodService,
                private menuService: MenuService,
                private router: Router,
                private dialog: MatDialog,
                public snackBar: MatSnackBar,
                public stepper: StepperService
                ) {
        this.basketService.basket.subscribe((basket: Basket) => {
            this.basket = basket;
        });
    }

    ngOnInit() {
    }

    amountChange() {
        this.basketService.setBasketValue(this.basket);
        this.basketService.basket.next(this.basket);

    }

    clearBasket() {
        if (confirm("Etes-vous sûr de vider le contenu de votre panier ? Cela vous ramènera à la page des films.")) {
            this.basketService.clear();
            this.router.navigate(['/']);
            this.snackBar.open( 'Le panier a été remis à zéro', '', {
                duration: 8000,
            });
        }
    }

    removeFoodGroup(index: number) {
       this.snackBar.open( 'La pizza ' + this.basket.foodGroups[index].product.nom + ' a été supprimée', '-' + this.basket.foodGroups[0].product.prix + '€', {
            duration: 8000,
        });
       if (this.basketService.getFoods().length < 2) {
            this.stepper.desactivEtat();
            this.stepper.activEtat()
            this.basketService.removeFoodGroupByIndex(index);
            this.router.navigate(['/product']);
        } else {this.basketService.removeFoodGroupByIndex(index); }
    }

    removeMenuGroup(index: number) {
        this.basketService.removeMenuGroupByIndex(index);
    }

    removeMovie(index: number) {
        if (confirm("Etes-vous sûr de vouloir modifier votre film ? Cela supprimera celui que vous avez actuellement.")) {
            this.snackBar.open( 'Le film ' + this.basket.movies[0].title + ' a été supprimé', '-5€', {
                duration: 8000,
            });
            this.basketService.removeMovieByIndex(index);
            this.stepper.desactivEtat();
            this.router.navigate(['/']);
        }
    }

    goToMoviePage(id: number) {
        const dialogRef = this.dialog.open(MovieInfoComponent, {
            width: '90vw',
            height: '70vh',
            data: id,
            closeOnNavigation: true
        });

        dialogRef.afterClosed().toPromise().then((productGroupInterface: ProductGroupInterface<any>) => {
        });
    }



    checkBasket() {
        let msg = '';
        if (this.basket.movies.length === 0) {
            msg += `Il vous faut au moins un film pour valider votre panier.`;
        }

        if (this.basket.foodGroups.length === 0 && this.basket.menuGroups.length === 0) {
            msg += '\n Il vous faut au moins une pizza pour valider votre panier. ';
        }

        if (msg !== '') {
            throw new Error(msg);
        }

        return msg;

    }


    submitBasket() {
        try {
            this.checkBasket();
            this.stepper.activFinish();
            this.router.navigate(['/bill']);

        } catch (error) {
            this.dialog.open(GenericDialogComponent, {
                width: '250',
                data: error
            });
        }


    }

}

