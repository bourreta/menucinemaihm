import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductSelectionComponent} from '../product/product-selection/product-selection.component';
import {ProductModule} from '../product/product.module';
import {MovieTableModule} from '../product/movie/movie-table/movie-table.module';
import {HomeComponent} from './home.component';
import {MatButtonToggleModule, MatIconModule, MatSnackBarModule} from '@angular/material';
import {StarRatingModule} from 'angular-star-rating';
import {SearchBarModule} from '../search-bar/search-bar.module';
import {AppModule} from '../app.module';
import {BasketModule} from '../basket/basket.module';
import { NavBarMovieComponent } from './nav-bar-movie/nav-bar-movie.component';
import { MovieCategoryFilterComponent } from './nav-bar-movie/movie-category-filter/movie-category-filter.component';
import { MovieStarFilterComponent } from './nav-bar-movie/movie-star-filter/movie-star-filter.component';
import { MovieInfoComponent } from '../dialogs/movie-info/movie-info.component';
import {MatDialogModule} from '@angular/material/dialog';


@NgModule({
    declarations: [HomeComponent, NavBarMovieComponent, MovieCategoryFilterComponent, MovieStarFilterComponent, MovieInfoComponent],
    exports: [HomeComponent, NavBarMovieComponent],
    imports: [
        CommonModule,
        ProductModule,
        MovieTableModule,
        MatIconModule,
        StarRatingModule,
        MatButtonToggleModule,
        SearchBarModule,
        BasketModule,
        MatDialogModule,
        MatSnackBarModule,
    ]
})
export class HomeModule {
}
