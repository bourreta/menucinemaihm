import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieCategoryFilterComponent } from './movie-category-filter.component';

describe('MovieCategoryFilterComponent', () => {
  let component: MovieCategoryFilterComponent;
  let fixture: ComponentFixture<MovieCategoryFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieCategoryFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieCategoryFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
