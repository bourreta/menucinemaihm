import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarMovieComponent } from './nav-bar-movie.component';

describe('NavBarMovieComponent', () => {
  let component: NavBarMovieComponent;
  let fixture: ComponentFixture<NavBarMovieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavBarMovieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarMovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
