import {Component, Input, OnInit} from '@angular/core';
import {TmdbService} from '../../../services/tmdb.service';
import {AuthService} from '../../../auth/auth.service';
import {Router} from '@angular/router';
import {NavBarService} from '../../../services/nav-bar.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-movie-star-filter',
  templateUrl: './movie-star-filter.component.html',
  styleUrls: ['./movie-star-filter.component.scss']
})
export class MovieStarFilterComponent implements OnInit {
  @Input() activeMenu: boolean;

  constructor(private tmdb: TmdbService,
              public authService: AuthService,
              private router: Router,
              private navbar: NavBarService,
              public snackBar: MatSnackBar,) {
  }

  ngOnInit() {
  }

  rating(rate) {
    if (this.navbar.searchRating === rate) {
      this.navbar.searchRating = 0;
      this.router.navigate(['/']);
      this.snackBar.open('La recherche par note a été réinitialisée.', 'OK', {
        duration: 8000,
      });
    }
    else {this.navbar.searchRating = rate;}
  }


}







