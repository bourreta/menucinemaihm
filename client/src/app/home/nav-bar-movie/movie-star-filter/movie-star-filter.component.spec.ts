import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieStarFilterComponent } from './movie-star-filter.component';

describe('MovieStarFilterComponent', () => {
  let component: MovieStarFilterComponent;
  let fixture: ComponentFixture<MovieStarFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieStarFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieStarFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
