import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NavBarService} from "../services/nav-bar.service";

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
    @Input() private searchValue = '';
    @Output() searchEvent: EventEmitter<string> = new EventEmitter();
    @Input() private hideSearchBar;
    private  countCat: number;
    constructor(private navBarService: NavBarService) {
        this.countCat = 0;
    }

    ngOnInit() {
        // évement qui se produit quand une recherche sur la catégorie action
        this.navBarService._hiddeSearchBarSubject.subscribe(
                data => {
                    //this.hideSearchBar = data;
                    if (data === true){
                        this.countCat ++;
                    } else{
                        this.countCat--;
                    }
                    if (this.countCat === 0){
                        this.hideSearchBar = false;
                    }else {this.hideSearchBar = true;}
                },
                err => {
                    console.log('error');
                },
                () => {
                }
            );
    }

    public onChange(target: any) {
        this.searchEvent.emit(target.value);
    }

}
